#!/bin/bash

set -o pipefail

clusterName="${1}"
if [[ -z "${clusterName}" ]]; then
  echo "Usage: $(basename "${0}") [cluster name]"
  exit 1
fi

baseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)
configSetting="${baseDir}/config/${clusterName}/setting.conf"
if [[ ! -f "${configSetting}" ]]; then
  echo "Error: ${configSetting} was not found"
  exit 1
fi

# shellcheck source=/dev/null
source "${configSetting}"

sshMain=(sshpass -p "${MAIN_PASSWORD}" ssh "${SSH_OPTS[@]}" "${MAIN_ACCOUNT}@${MAIN_IP}")

debug_mode() {
  if [[ "${DEBUG_MODE}" == "true" ]]; then
    echo -e "DEBUG: ${1} \ \n${2}"
  fi
}

initialize_k8s_cluster() {
  # https://github.com/kube-vip/kube-vip/issues/907
  local sshCmd="\
  sudo podman exec -it ${MAIN_CONTROL_PLANE} bash -c '\
    sed -i \"s|path: /etc/kubernetes/admin.conf|path: /etc/kubernetes/super-admin.conf|g\" \
      /etc/kubernetes/manifests/kube-vip.yaml &>/dev/null || true
    kubeadm init --config=\$(find /root -name init.yaml) &>/dev/null
    sed -i \"s|path: /etc/kubernetes/super-admin.conf|path: /etc/kubernetes/admin.conf|g\" \
      /etc/kubernetes/manifests/kube-vip.yaml &>/dev/null || true'"

  debug_mode "${sshMain[*]}" "${sshCmd}"
  if ! "${sshMain[@]}" "${sshCmd}"; then
    echo "Error: Failed to initialize Kubernetes"
    exit 1
  fi
  echo "Info: Successfully initialized Kubernetes"
}

setup_kubeconfig() {
  local sshCmd="\
  mkdir -p ~/.kube 2>/dev/null
  sudo podman cp ${MAIN_CONTROL_PLANE}:/etc/kubernetes/admin.conf ~/.kube/config --overwrite
  sudo podman cp ${MAIN_CONTROL_PLANE}:/usr/bin/kubectl /usr/local/bin/kubectl --overwrite
  sudo chown \$(id -u):\$(id -g) ~/.kube/config
  sudo podman exec -it ${MAIN_CONTROL_PLANE} bash -c '\
    mkdir ~/.kube 2>/dev/null
    cp -uf /etc/kubernetes/admin.conf ~/.kube/config
    chown \$(id -u):\$(id -g) ~/.kube/config'"

  debug_mode "${sshMain[*]}" "${sshCmd}"
  if ! "${sshMain[@]}" "${sshCmd}"; then
    echo "Error: Kube config setup failed on [${MAIN_CONTROL_PLANE}] on [${MAIN_IP}]"
    exit 1
  fi
}

# main
initialize_k8s_cluster
setup_kubeconfig
