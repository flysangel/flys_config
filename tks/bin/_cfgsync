#!/bin/bash

set -o pipefail

clusterName="${1}"
if [[ -z "${clusterName}" ]]; then
  echo "Usage: $(basename "${0}") [cluster name]"
  exit 1
fi

baseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)
configSetting="${baseDir}/config/${clusterName}/setting.conf"
if [[ ! -f "${configSetting}" ]]; then
  echo "Error: ${configSetting} was not found"
  exit 1
fi

# shellcheck source=/dev/null
source "${configSetting}"

debug_mode() {
  if [[ "${DEBUG_MODE}" == "true" ]]; then
    echo -e "DEBUG: ${1} \ \n${2}"
  fi
}

generate_config() {
  configInit="${baseDir}/config/${clusterName}/init.tpl"
  if [[ ! -f "${configInit}" ]]; then
    echo "Error: ${configInit} was not found"
    exit 1
  fi

  if ! envsubst <"${configInit}" >"${baseDir}/config/${clusterName}/init.yaml"; then
    echo "Error: Failed to generate init.yaml"
    exit 1
  fi
}

parse_container_info() {
  IFS='|' read -r ip account password k8sComponent _ containerName _ _ _ <<<"${line}"
  ssh=(sshpass -p "${password}" ssh "${SSH_OPTS[@]}" "${account}@${ip}")
}

scp_config_to_ip() {
  if [[ "${k8sComponent}" != "control-plane" ]]; then
    return
  fi

  if ! sshpass -p "${password}" \
    scp "${SSH_OPTS[@]}" \
    "${baseDir}/config/${clusterName}/init.yaml" \
    "${account}@${ip}:/tmp/init.yaml"; then
    echo "Error: Failed to SCP [init.yaml] to [${ip}]"
    exit 1
  fi

  if ! sshpass -p "${password}" \
    scp -r "${SSH_OPTS[@]}" \
    "${baseDir}/mks" \
    "${account}@${ip}:/tmp/mks"; then
    echo "Error: Failed to SCP [mks] to [${ip}]"
    exit 1
  fi
}

copy_config_to_container() {
  if [[ "${k8sComponent}" != "control-plane" ]]; then
    return
  fi

  local sshCmd="\
  sudo podman exec -it ${containerName} bash -c 'rm -rf /root/mks'
  sudo podman cp /tmp/init.yaml ${containerName}:/root --overwrite
  sudo podman cp /tmp/mks ${containerName}:/root --overwrite
  rm -rf /tmp/init.yaml /tmp/mks"

  debug_mode "${ssh[*]}" "${sshCmd}"
  if ! "${ssh[@]}" "${sshCmd}"; then
    echo "Error: Failed to copy [init.yaml] [mks] to [${containerName}] on [${ip}]"
    exit 1
  fi
}

setup_kube_vip() {
  if [[ "${k8sComponent}" != "control-plane" || -z "${VIRTUAL_IP_ADDRESS}" ]]; then
    return
  fi

  local sshCmd="\
  sudo podman exec -it ${containerName} bash -c '\
    cp -uf \$(find /root -name *-kube-vip.yaml -type f) /etc/kubernetes/manifests/kube-vip.yaml
    sed -i -e \"s/192.168.0.1/${VIRTUAL_IP_ADDRESS}/g;s/eth0/\$(ip route | awk \"/default/ {print \\\$5}\")/g\" \
      /etc/kubernetes/manifests/kube-vip.yaml'"

  debug_mode "${ssh[*]}" "${sshCmd}"
  if ! "${ssh[@]}" "${sshCmd}"; then
    echo "Error: Failed to set up kube-vip on [${ip}] on [${containerName}]"
    exit 1
  fi
}

# main
generate_config

IFS=$'\n' read -d '' -r -a TK8S_ARRAY <<<"${TK8S_LISTS}"
for line in "${TK8S_ARRAY[@]}"; do
  parse_container_info
  scp_config_to_ip
  copy_config_to_container
  setup_kube_vip
done
