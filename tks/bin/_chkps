#!/bin/bash

set -o pipefail

clusterName="${1}"
if [[ -z "${clusterName}" ]]; then
  echo "Usage: $(basename "${0}") [cluster name]"
  exit 1
fi

baseDir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)
configSetting="${baseDir}/config/${clusterName}/setting.conf"
if [[ ! -f "${configSetting}" ]]; then
  echo "Error: ${configSetting} was not found"
  exit 1
fi

# shellcheck source=/dev/null
source "${configSetting}"

sshMain=(sshpass -p "${MAIN_PASSWORD}" ssh "${SSH_OPTS[@]}" "${MAIN_ACCOUNT}@${MAIN_IP}")

debug_mode() {
  if [[ "${DEBUG_MODE}" == "true" ]]; then
    echo -e "DEBUG: ${1} \ \n${2}"
  fi
}

parse_container_info() {
  IFS='|' read -r ip account password _ _ containerName _ _ _ <<<"${line}"
  ssh=(sshpass -p "${password}" ssh "${SSH_OPTS[@]}" "${account}@${ip}")
}

check_container_status() {
  local sshCmd="\
  sudo podman ps --filter name='^${containerName}\$' --format {{.State}}
  "

  debug_mode "${ssh[*]}" "${sshCmd}"
  if ! state=$("${ssh[@]}" "${sshCmd}"); then
    echo "Error: Failed to check container [${containerName}]"
    exit 1
  fi

  if [[ -z "${state}" ]]; then
    echo "Error: Failed to check container [${containerName}] [not running]"
    return
  fi
  echo "Info: Successfully checked container [${containerName}] [${state}]"
}

# main
IFS=$'\n' read -d '' -r -a TK8S_ARRAY <<<"${TK8S_LISTS}"
for line in "${TK8S_ARRAY[@]}"; do
  parse_container_info
  check_container_status
done
