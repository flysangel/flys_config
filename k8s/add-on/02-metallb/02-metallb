#!/bin/bash
if ! sudo -l -U "$(whoami)" &>/dev/null; then
  echo "Sorry, user $(whoami) is not allowed to execute 'sudo' on $(hostname)" && exit 1
fi

if ! command -v kubectl &>/dev/null; then
  echo 'Please install "kubectl"' && exit 1
fi

baseDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
yamlName=$(echo "${baseDir}" | awk -F'/' '{print $NF}')

case "${1-}" in
apply)
  kubectl apply -f "${baseDir}"/"${yamlName}".yaml 2>/dev/null
  echo "Please wait ..."
  sleep $((5 + RANDOM % 7))
  export netId=$(kubectl get node -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}' | tr ' ' '\n' | grep -oP '\K(\w+.){3}' | sort -u)
  kubectl get -n kube-system configmap kube-proxy -o yaml 2>/dev/null | sed 's/strictARP: false/strictARP: true/g' | kubectl apply -f - 2>/dev/null
  kubectl wait -n metallb-system pod -l component=speaker --for=condition=Ready --timeout=360s &&
    kubectl wait -n metallb-system pod -l component=controller --for=condition=Ready --timeout=360s &&
    envsubst <"${baseDir}"/"${yamlName}"-ippools.yaml | kubectl apply -f - 2>/dev/null &&
    echo "Install ... successed" ||
    echo "Install ... failed"
  ;;

delete)
  kubectl delete -f "${baseDir}"/"${yamlName}"-ippools.yaml 2>/dev/null
  kubectl delete -f "${baseDir}"/"${yamlName}".yaml 2>/dev/null
  ;;

pull)
  grep -oP '[^#]image: \K.*' "${baseDir}"/*.yaml | sort -u | xargs -I {} sudo crictl pull {} 2>/dev/null
  ;;

esac
