#!/bin/bash

RAW='https://gitlab.com/flysangel/srv_config/raw/main'

echo '' >README.md
find . -name 'model_name' -type f -exec rm -rf {} +

find . -name '*.txt' -type f |
  while read -r line; do
    os=$(echo "${line}" | awk -F'/' '{print $2}')
    class=$(echo "${line}" | awk -F'/' '{print $3}')
    pkg=$(echo "${line}" | awk -F'/' '{print $4}')
    {
      echo "# ${os} ${class} ${pkg}"
      echo ''
      echo '```bash'
      echo "wget -qO - ${RAW}/${os}/${class}/${pkg}/${pkg}.txt | sudo bash"
      echo '```'
      echo ''
    } >>README.md

    echo "${class}.${pkg}" >>"${os}"/model_name
  done
