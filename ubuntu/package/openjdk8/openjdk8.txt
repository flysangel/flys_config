#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# install java
apt update --fix-missing
DEBIAN_FRONTEND=noninteractive apt install -y openjdk-8-jdk openjdk-8-jre
