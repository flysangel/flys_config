#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

XRDP_USER=desktop
XRDP_PASSWD=desktop520

# install aconfig
wget -qO - https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/aconfig/aconfig.txt | bash

# install xfce
apt update --fix-missing
DEBIAN_FRONTEND=noninteractive apt upgrade -y
DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends xfce4 xinit
DEBIAN_FRONTEND=noninteractive apt install -y \
  xrdp xtightvncviewer \
  firefox mousepad xfce4-terminal \
  fcitx5 fcitx5-chewing fcitx5-chinese-addons numlockx \
  ttf-wqy-zenhei language-pack-zh* language-pack-gnome-zh* \
  zenity xterm

# fix xrdp
# https://www.c-nergy.be/products.html
adduser xrdp ssl-cert
sed -i 's/allowed_users=console/allowed_users=anybody/' /etc/X11/Xwrapper.config
grep -Fxq 'session required pam_env.so readenv=1 user_readenv=0' /etc/pam.d/xrdp-sesman ||
  sed -i '1 a session required pam_env.so readenv=1 user_readenv=0' /etc/pam.d/xrdp-sesman
wget https://github.com/neutrinolabs/xrdp/raw/devel/sesman/startwm.sh -qO /etc/xrdp/startwm.sh

# clean
apt purge -y htop* info* byobu* ibus*
apt autoremove -y
apt clean -y
apt autoclean -y
rm -rf /var/lib/apt/lists/*

# remove apps
mkdir -p /usr/share/applications
for app in vim debian-uxterm debian-xterm \
  thunar thunar-bulk-rename thunar-settings \
  xfce4-mail-reader xfce4-file-manager xfce4-about; do
  mv /usr/share/applications/${app}.desktop /usr/share/applications/${app}.desktop.bk
done

# create user
useradd -m -s /bin/bash ${XRDP_USER}
usermod -a -G adm,sudo ${XRDP_USER}
echo "${XRDP_USER}:${XRDP_PASSWD}" | chpasswd
chown -R ${XRDP_USER}:${XRDP_USER} /home/${XRDP_USER}
chmod 700 /home/${XRDP_USER}

# set profile
mv /etc/xdg/xfce4/panel/default.xml /etc/xdg/xfce4/panel/default.xml.bk
mv /etc/xdg/xfce4/helpers.rc /etc/xdg/xfce4/helpers.rc.bk
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/etc/xdg/xfce4/helpers.rc -qO /etc/xdg/xfce4/helpers.rc
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/etc/xdg/xfce4/panel/default.xml -qO /etc/xdg/xfce4/panel/default.xml
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml -qO /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/etc/xdg/autostart/fcitx.desktop -qO /etc/xdg/autostart/fcitx.desktop

mkdir -p /home/${XRDP_USER}/.config/fcitx5
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/config/fcitx5/profile -qO /home/${XRDP_USER}/.config/fcitx5/profile
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/package/xfce/config/fcitx5/config -qO /home/${XRDP_USER}/.config/fcitx5/config
chown -R ${XRDP_USER}:${XRDP_USER} /home/${XRDP_USER}
