#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# update and install
apt update --fix-missing
DEBIAN_FRONTEND=noninteractive apt install -y locales

# locale
locale-gen en_US en_US.UTF-8 zh_TW.UTF-8
locale -a &>/dev/null

# set locale to file
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/locale/locale -qO /etc/default/locale

# set keyboard to file
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/locale/keyboard -qO /etc/default/keyboard

# language
wget https://gitlab.com/flysangel/srv_config/raw/main/ubuntu/system/locale/locale-env.sh -qO /etc/profile.d/locale-env.sh
