#!/bin/bash
if ! ls /root &>/dev/null; then
  echo 'Please use "sudo bash" run' && exit 1
fi

if ! grep -q 'Ubuntu.*LTS' /etc/os-release; then
  read -rp 'Ops, Not UBUNTU LTS, Press any key to continue.' && exit 1
fi

# good time good start
sleep $((1 + RANDOM % 3))

#set -x # debug mode
set -a # var export

while true; do
  # network interface
  GW=$(ip route | awk '/default/ {print $3}')
  GWIF=$(ip route | awk '/default/ {print $5}')
  IP=$(ip a show "${GWIF}" | awk '/inet / {print $2}' | cut -d'/' -f1)
  MAC=$(ip a show "${GWIF}" | awk '/ether/ {print $2}')
  NETID=$(ip a show "${GWIF}" | awk '/inet / {print $2}' | cut -d'.' -f1-3)
  NETMASK=$(ip a show "${GWIF}" | awk '/inet / {print $2}' | cut -d'/' -f2)

  # dialog env
  hostName=$(hostname)
  nowTime=$(date +"%Y-%m-%d %H:%M:%S")
  upTime=$(uptime -p)
  cpu=$(awk -F': ' '/model name/ {print $2; exit}' /proc/cpuinfo)
  cpuCore=$(grep -c '^processor' /proc/cpuinfo)
  cpuUsed=$(awk '/^cpu / {usage=($2+$4)*100/($2+$4+$5); print usage"%"}' /proc/stat)
  mem=$(awk '/Mem/ {print $2}' <(free -h))
  memUsed=$(awk '/Mem/ {print $3}' <(free -h))
  df=$(df -h / | awk 'NR==2 {print $2}')
  dfUsed=$(df -h / | awk 'NR==2 {print $3}')
  ns=$(awk '/^nameserver/ {print $2; exit}' /etc/resolv.conf)
  ping -c 1 -W 1 "${GW}" &>/dev/null && gwping='OK' || gwping='Deny'
  ping -c 1 -W 1 "${ns}" &>/dev/null && nsping='OK' || nsping='Deny'

  # dialog output
  envsubst </mks/dialog/00.tmp | tee /mks/dialog/00.info &>/dev/null
  rm -rf /mks/dialog/mks.dialog &>/dev/null
  for info in /mks/dialog/*.info; do
    tee -a /mks/dialog/mks.dialog <"${info}" &>/dev/null
    echo '' | tee -a /mks/dialog/mks.dialog &>/dev/null
  done

  # !!! script !!!
  for script in /mks/script/*.sh; do
    (. "${script}" &>/dev/null)
  done

  # good time good start
  sleep $((20 + RANDOM % 5))
done
